<?php

class TjeuxPC_titre
	{
	public $os='';
	public $titre='';
	public $ver='';
	public $url='';
	public $img='';
	public $description='';

	public function TjeuxPC_titre($title)
		{
		$this->titre=$title;
		}

	}

class TjeuxPC
	{
	public $jeux;
	public $jeuxNb;

	public function TjeuxPC()
		{
		$this->jeuxNb=0;
		$this->jeux=array();
		}

	public function add($title)
		{
		 if(@!$this->jeux[$title])
			{
			$this->jeux[$title]=new TjeuxPC_titre($title);
			$this->jeuxNb++;
			}
		}
	public function tableau()
		{
		$out='';
		$out.='<table>';
		$out.='<tr><th>img</th><th>os</th><th>titre</th><th>ver</th><th>url</th><th>description</th></tr>';
		foreach($this->jeux as $key => $value) 
			{
			$out.="<tr><td><a href='{$this->jeux[$key]->img}'><img width='50px' alt=''src='{$this->jeux[$key]->img}'></a></td>"
				."<td>{$this->jeux[$key]->os}</td><td>{$this->jeux[$key]->titre}</td>"
				."<td>{$this->jeux[$key]->ver}</td>"
				."<td><a href='{$this->jeux[$key]->url}'>lien</a></td><td>{$this->jeux[$key]->description}</td></tr>";
			}
		$out.='</table>';
		return $out;
		}

	}

$jeuxListe=new TjeuxPC();
$jeuxListe->add('Caph');
	$jeuxListe->jeux['Caph']->os='Linux';
	$jeuxListe->jeux['Caph']->url='http://doc.ubuntu-fr.org/caph';
	$jeuxListe->jeux['Caph']->img='http://sourceforge.net/dbimage.php?id=245680';

$jeuxListe->add('playdeb');
	$jeuxListe->jeux['playdeb']->os='Linux';
	$jeuxListe->jeux['playdeb']->url='http://www.playdeb.net/updates/ubuntu/10.04/';
	$jeuxListe->jeux['playdeb']->img='';
	$jeuxListe->jeux['playdeb']->description='plateforme de jeux pour debian';

$jeuxListe->add('powdertoy');
	$jeuxListe->jeux['powdertoy']->os='Linux|win|mac';
	$jeuxListe->jeux['powdertoy']->url='http://powdertoy.co.uk/';
	$jeuxListe->jeux['powdertoy']->img='http://bbsimg.ali213.net/attachments/day_090212/20090212_e947792031102ef091be7NBMnxj3Cglq.jpg';
	$jeuxListe->jeux['powdertoy']->description='simulation d\'environnement de physique';

$jeuxListe->add('ctr');
	$jeuxListe->jeux['ctr']->titre='Cute The Rope';
	$jeuxListe->jeux['ctr']->os='html5';
	$jeuxListe->jeux['ctr']->url='http://www.cuttherope.net/';
	$jeuxListe->jeux['ctr']->img='https://encrypted-tbn3.google.com/images?q=tbn:ANd9GcRfTzC8zc7WzAd4prnikowxPu7z8ommA-kRWPbwIleFrGnDOJFe';
	$jeuxListe->jeux['ctr']->description='jeux d\'adresse et de rapidit&eacute;';

?>
<h1>Les jeux sous Linux</h1>
<?php echo $jeuxListe->tableau();?>

