<h1>ludotheques - liste des jeux</h1>
<?php
//definition constantes et variables communes
//include_once('/www/sites/intersites/lib/perso/php/leGite-general/leGite-general-1.0.inc.php');

function UTF8ToEntities($string) {
	$string = str_replace('é','&eacute;',$string);
	return $string;
}
function get_correct_utf8_mysql_string($s)
{
if(empty($s)) return $s;
$s = preg_match_all("#[\x09\x0A\x0D\x20-\x7E]|[\xC2-\xDF][\x80-\xBF]|\xE0[\xA0-\xBF][\x80-\xBF]|[\xE1-\xEC\xEE\xEF][\x80-\xBF]{2}|\xED[\x80-\x9F][\x80-\xBF]#x",$s,$m);
return implode("",$m[0]);
}


include_once(DOCUMENT_ROOT.'mysqlPHP/legral_databases.php');

$dbh= legral_database_Connect ($legralDBSelect='legralovh');
if (!is_object($dbh))
	echo ("Erreur &agrave; la connexion de la database '$legralDBSelect'  | Error = $dbh");
//else	echo ("connexion reussi");

define('TRICTRAC_JEUXPAGE_URL','http://www.trictrac.net/index.php3?id=jeux&rub=detail&inf=detail&jeu=');
define('TRICTRAC_JEUXIMG_URL','http://www.trictrac.net/jeux/centre/imagerie/boites/');//8698_1.jpg

function level_getImage($level){return("/intersites/images/points/barre_$level.gif");}
function trictrac_jeuxImg_URL($jeuxNu){return(TRICTRAC_JEUXIMG_URL."$jeuxNu".'_1.jpg');}
?>

<a name='liste'></a>
<div id='liste_detail' style='overflow:auto;font-size:small;margin-top:1em;background-color:#bbb;'>

<?php
$dbRQ = $dbh->query('SELECT COUNT(*) AS jeuxNb FROM Tjeux');
while ($dbRow = empty($dbRQ)?false:$dbRQ->fetch(PDO::FETCH_NAMED)){$jeuxNb=$dbRow['jeuxNb'];}
echo "nombre de jeux totale:$jeuxNb<br />";
if (!empty($dbRQ))	$dbRQ->closeCursor();
?>
<style type="text/css">
#table_liste_jeux {margin-left:auto;margin-right:auto;}
#table_liste_jeux thead th a{color:#E05;}
#table_liste_jeux tbody tr{text-align:right;white-space:nowrap;}
#table_liste_jeux tbody tr.paire {background-color:#FFCC00;}
#table_liste_jeux tbody tr.impaire{background-color:#AAA;}
#table_liste_jeux tbody td{text-align:right;white-space:nowrap;}
</style>
<table  id='table_liste_jeux'>
<caption></caption>
<thead><tr><?php
// **** entete du tableau ****
$colonne = array('refTricTrac','nom','image_url','annee','genre','duree','joueurNb_max','difficulte','chance','strategie','diplomatie');
foreach($colonne as &$value){echo"<th><a href='?page=plateau&amp;trie=$value#liste' title='trier par $value'>$value</a></th>";}
//mecanisme
$colonne = array('deduction','cooperatif','enchere','placement','simultanne','Construction');
echo'<th>';
	foreach ($colonne as &$value){echo"<a href='?page=plateau&amp;mecanisme=$value#liste'title='selectionner le mecanisme: $value'>$value</a> ";}
echo'</th>';
$colonne=array('auteurs','editeurs','distributeur');
foreach($colonne as &$value){echo"<th><a href='?page=plateau&amp;trie=$value#liste' title='trier par $value'>$value</a></th>";}
?></tr></thead><tbody>
<?php
// **** corps du tableau ****
$mecanisme= (isset($_GET['mecanisme']))?$_GET['mecanisme']:'';
	switch ($mecanisme)
		{
		case 'deduction':case 'cooperatif':case 'enchere':case 'placement':case 'simultanne':case 'Construction':$mecanisme='mecanisme_'.$mecanisme;break;
//		default:break;
		}

$trie= (isset($_GET['trie']))?$_GET['trie']:'nom';	// toujours trie par nom par defaut
$where='';
if ("$mecanisme" !=''){$where="WHERE $mecanisme=1";}
$order_by=" ORDER BY $trie";

$sql=	'SELECT refTricTrac,nom,image_url,image_data,auteurs,editeurs,distributeur,annee,genre,duree,joueurNb_min,joueurNb_max'
		.	',difficulte,chance,strategie,diplomatie'
		.	',mecanisme_deduction,mecanisme_cooperatif,mecanisme_enchere,mecanisme_placement,mecanisme_simultanne,mecanisme_Construction'
		.	' FROM `Tjeux`'
		.	" $where"
		.	" $order_by";
//echo "sql:$sql<br />";
$dbRQ = $dbh->query($sql);
$ligne=0;
while ($dbRow = empty($dbRQ)?false:$dbRQ->fetch(PDO::FETCH_NAMED))
	{

	//calcul des variables
	if ($dbRow['refTricTrac'])
		{$jeuxImg_URL=trictrac_jeuxImg_URL($dbRow['refTricTrac']);}
	else	{$jeuxImg_URL=$dbRow['image_url'];}

	//entete
	$class= ($ligne++%2)?'paire':'impaire';
	echo'<tr class="'.$class.'">';

	$class='';
	$jeux_nom=htmlentities($dbRow['nom'],ENT_QUOTES);
	$tt_jeux_pageURL=TRICTRAC_JEUXPAGE_URL.$dbRow['refTricTrac'];
//	echo"<td style='text-align:center;'>{$dbRow['jeuxRef']}</td>";
	echo"<td><a alt='$jeux_nom' sur trictrac' title='$jeux_nom sur trictrac' href='$tt_jeux_pageURL'>{$dbRow['refTricTrac']}</a></td>";
	echo"<td style='text-align:left;'>$jeux_nom</td>";
	echo"<td style='text-align:center;'> <a href='$jeuxImg_URL'><img alt='' title='voir en grand' style='weight:30px;width:30px;' src='$jeuxImg_URL' /></a></td>";
//	echo"<td>{$dbRow['image_data']}</td>";
	echo"<td style='text-align:center;'>{$dbRow['annee']}</td>";
	echo"<td style='text-align:center;'>{$dbRow['genre']}</td>";
	echo"<td style='text-align:center;'>{$dbRow['duree']}</td>";
	echo"<td style='text-align:center;'>de {$dbRow['joueurNb_min']} &agrave; {$dbRow['joueurNb_max']}</td>";
	echo'<td><img alt="" src="'.level_getImage($dbRow['difficulte']).'" /></td>';
	echo'<td><img alt="" src="'.level_getImage($dbRow['chance']).'" /></td>';
	echo'<td><img alt="" src="'.level_getImage($dbRow['strategie']).'" /></td>';
	echo'<td><img alt="" src="'.level_getImage($dbRow['diplomatie']).'" /></td>';

	echo'<td style="text-align:left;">'
		. (($dbRow['mecanisme_deduction']==1)?' deduction ':'')
		. (($dbRow['mecanisme_enchere']==1)?' enchere':'')
		. (($dbRow['mecanisme_placement']==1)?' placement':'')
		. (($dbRow['mecanisme_simultanne']==1)?' simultanne':'')
		. (($dbRow['mecanisme_Construction']==1)?' Construction':'')
		.'</td>';

	echo"<td style='text-align:left;'>{$dbRow['auteurs']}</td>";
	echo"<td style='text-align:left;'>{$dbRow['editeurs']}</td>";
	echo"<td style='text-align:left;'>{$dbRow['distributeur']}</td>";

	echo'</tr>';
	}
if (!empty($dbRQ))	$dbRQ->closeCursor();
?>
</tbody></table></div>
