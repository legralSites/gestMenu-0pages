<!--script src="/intersites/lib/tiers/js/prototype/prototype-1.7.0.0.js"></script-->
<script>gestLib.loadLib({nom:'consoleVie',ver:'0.2.2',description:'consoles de signes vitaux'});</script>

<style>
body
	{
	background-color: rgb(255, 255, 255);
	background-image: url(../img/Ciel053.jpg);
	}
#dimention1CSS
	{
	position:relative;
/*	left:5px;top:300px;*/
	width:750px;height:400px;
	border:1px red solid;
	background: #777 url(/intersites/lib/perso/js/dimentions/img/Ciel053.jpg) repeat left top;
	overflow:'hidden';
	}
#message
	{
	position:absolute;
	left:5px;top:400px;
	width:900px;height:200px;
	border:1px red solid;
	background: #999 url() repeat left top;
	overflow:'hidden';
	}

.ruchier_img
	{
	position:relative;
	left:0px;
	top:0px;
	width:32px;
	height:32px;
	visibility:visible;
	}
</style>

<script src="/intersites/lib/perso/js/intersites-general/intersites-general-1.2.1.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/dimention-elt-independant.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/cycle-0.1.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/dimention-core-0.2.2.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/dimention-elt-prototype0.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/dimention-elt-comportement-0.2.2.js"></script>

<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/elt/attention-vie.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/elt/avion-vie.js"></script>
<script src="/intersites/lib/perso/js/dimentions/dimentions-0.1/elt/ravitailleur-vie.js"></script>
<script>
function cycleDureeDec()
	{
	;
	}
function cycleDureeInc()
	{
	dimention1.cycle.consigne++;
	}
</script>
<body id="body">
<h1>Ruche <span id="ruches_version"></span></h1>
<div id="dimention1CSS" title="dimention1"></div>
<span style="cursor:pointer;" onclick="dimention1.cycle.consigne-=10">-10</span>
 <span style="cursor:pointer;" onclick="dimention1.cycle.consigne--">-1</span>
 consigne (ms)
 <span style="cursor:pointer;" onclick="dimention1.cycle.consigne++">+1</span>
<span style="cursor:pointer;" onclick="dimention1.cycle.consigne+=10">+10</span>

<!-- consoles de monitoring de vie -->
<div id="consoleVie_Menu"></div>
<div id="consoleVie_Support">
	<span id="consoleVie_Nom"class="console_Nom"    onclick="javascript:gestLib.switchShow('consoleVie');">Nom</span>
	<span id="consoleVie_Clear"class="console_Nom"  onClick="javascript:gestLib.clear('consoleVie');">Effacer</span>
	<div id="consoleVie_Texte"class="console_Texte">Texte</div>
</div>

<!-- consoles de debug -->
<div><a class="pointeur" onclick="javascript:d=document.getElementById('debug_support');d.style.display=(d.style.display==='none')?'block':'none';">+ debug</a>

<div id="debug_support" style="display:none;">

<div id="dimention_Support">
	<span id="dimention_Nom"class="console_Nom"    onclick="javascript:gestLib.switchShow('dimention');">Nom</span>
	<span id="dimention_Clear"class="console_Nom"  onClick="javascript:gestLib.clear('dimention');">Effacer</span>
	<div id="dimention_Texte"class="console_Texte">Texte</div>
</div>

<hr /><p><h2>Librairies utilisées:</h2>
<?php //global $gestLib;echo $gestLib->libTableau();?>
<script>
document.write(gestLib.tableau());
//document.write(gestAjax.tableau());
dimention1=new gestDimention({idHTML:'dimention1CSS',cycle:{consigne:100,avg_pointsMax:200,callback:'dimention1.vie()'},CSS:{width:500,height:200}});
</script>
</div></div>
</body>
<script>

//**********************************************************************************************/
function ruchier_init()
	{
	document.getElementById("ruches_version").innerHTML=ruches_version;
	// ----------- ajouter elt: abeilles1 ---------------
	with(dimention1)
		{
		eltAdd({statut:1,idJS_parent:dimention1,idHTML:'prototype',	genre:'prototype',	imgClasse:'ruchier_img',coordX:001,coordY:120,vitesse:1,vecteurX:5,vecteurY:2,haut:43,larg:57, CeC:1,src:'/intersites/lib/perso/js/dimentions/img/Abeille2.gif'})
		
		eltAdd({statut:1,idJS_parent:dimention1,idHTML:'attention1',	genre:'panneau',	imgClasse:'ruchier_img',coordX:100,coordY:010,		 vecteurX:1,vecteurY:5,haut:30,larg:40, CeC:0,src:'/intersites/lib/perso/js/dimentions/img/attention-2895.png'})
		elt_attention_load(elt['attention1']);

		eltAdd({statut:1,idJS_parent:dimention1,idHTML:'attention2',	genre:'panneau',	imgClasse:'ruchier_img',coordX:300,coordY:050,vecteurX:3,vecteurY:1,haut:30,larg:40, CeC:0,src:'/intersites/lib/perso/js/dimentions/img/attention-2895.png'})
		elt_attention_load(elt['attention2']);

		eltAdd({statut:1,idJS_parent:dimention1,idHTML:'avion1',		genre:'avion',	imgClasse:'ruchier_img',coordX:350,coordY:120,vecteurX:1,vecteurY:0,haut:43,larg:57, CeC:0,src:'/intersites/lib/perso/js/dimentions/img/avion.png'})
		elt_avion_load(elt['avion1']);

		eltAdd({statut:1,idJS_parent:dimention1,idHTML:'ravitailleur1',	genre:'ravitailleur',	imgClasse:'ruchier_img',coordX:Math.round(dimention1.idCSS.style.width/2),coordY:Math.round(dimention1.idCSS.style.height/2),vitesse:1,vecteurX:1,vecteurY:0,haut:43,larg:57, CeC:2,src:'/intersites/images/pictogrammes/collection1/120px-EA-18G.svg.png'})
		elt_ravitailleur_load(elt['ravitailleur1']);
		}
	}
ruchier_init();

gestLib.end('dimention');gestLib.setConsole('dimention');//gestLib.clear('dimention');
//gestLib.write({lib:'dimention',txt:'gestLib:dimention Crée'});
gestLib.end('consoleVie');gestLib.setConsole('consoleVie');//gestLib.clear('consoleVie');
//gestLib.write({lib:'consoleVie',txt:''});

var t='';
//creation du bouton de pause
t+='<span style="cursor:pointer;border:1px solid red;"'+'onclick="javascript:dimention1.cycle.setPause();gestLib.write({lib:\'dimention\',txt:\'pause:'+dimention1.cycle.isPause+'\'});">switchPause</span> ';
t+='<span style="cursor:pointer;border:1px solid red;"'+'onclick="javascript:dimention1.cycle.setPause(0);gestLib.write({lib:\'dimention\',txt:\'pause:'+dimention1.cycle.isPause+'\'});dimention1.vie();">pause0</span> ';
t+='<span style="cursor:pointer;border:1px solid red;"'+'onclick="javascript:dimention1.cycle.setPause(1);gestLib.write({lib:\'dimention\',txt:\'pause:'+dimention1.cycle.isPause+'\'});">pause1</span> ';

//creation des boutons de monitoring
t+='<span style="cursor:pointer;border:1px solid red;"onclick="javascript:dimention1.afficheInfoNo=\'\'">dimention1</span> ';
for(var i in dimention1.elt){t+='<span style="cursor:pointer;border:1px solid red;"onclick="javascript:dimention1.afficheInfoNo=\''+dimention1.elt[i].idHTML+'\'">'+dimention1.elt[i].idHTML+'</span> ';}
document.getElementById('consoleVie_Menu').innerHTML=t;
//Lib.write({lib:'consoleVie_Menu',t:''});
delete t;

</script></html>
