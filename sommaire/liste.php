<title>liste des articles</title>
<style>.liste{margin:.2em;border:1px solid #0A0;background-color:#03BA03;padding:.5em}</style>
<?php
global $gestLib,$legralUsers,$listeType;
$sommaireRepNb=0;
$sommaireFicNb=0;
/*
liste les rubriques (repertoires) et articles (fichiers) contenu dans FICHIER_ROOT
$rep: repertoire racine a scanner
Ne pas utiliser utiliser pour la :
 - rubNiv: niveau de recursivite actuel
*/
function listeRep($rep=FICHIER_ROOT,$rubNiv=0)
	{
//	if(strrpos($rep,'tiers'))return;
	global $gestLib,$legralUsers,$listeType,$sommaireRepNb,$sommaireFicNb;
//echo	"nombre de rep:$sommaireRepNb<br>nombre de fic:$sommaireFicNb<br>";

	$out='';
	if($rubNiv===1)
		{
	//	$rub=array();
		$out.='<ul style="">';
		}
	else{$out.='<ul>';}
	if ($handle=opendir($rep))
		{
		while(FALSE!==($entry=readdir($handle)))
			{
			if($entry!='.'&&$entry!='..')
				{
				if($entry==='sommaire')continue;
				$e=$rep.'/'.$entry;
				$repRel=str_replace(FICHIER_ROOT,'',$e);
//				echo $gestLib->debugShowVar('legral-sommaire',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'FICHIER_ROOT',FICHIER_ROOT);
//				echo $gestLib->debugShowVar('legral-sommaire',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$rep',$rep);
//				echo $gestLib->debugShowVar('legral-sommaire',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$repRel',$repRel);
//				echo $gestLib->debugShowVar('legral-sommaire',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$e',$e);

				$style='';
				$style.='font-weight:';
				$style.=(is_dir($e))?'bold;':'normal;';

				if($rubNiv===1){$style.='margin:0;border:0;';}
				if($rubNiv>=2)
					{	
  					if(is_dir($e)){$style.='margin:.5em 0;border:1px solid blue;';}
					else{$style.='';}
					}

//				if($rubNiv>=1){$style.='font-weight:normal;';}

				$out.="<li style='list-style:none;$style'>";

				if(is_dir($e))
					{
					$sommaireRepNb++;
					$out.="$entry";
					$out.=listeRep($e,$rubNiv+1);
					}
				else
					{
					$sommaireFicNb++;
					$el=substr($e,strlen(FICHIER_ROOT)+1,strlen($e)-1);
					$out.="$entry (<a href='?page=lire&listeType=$listeType&article=$el'>lire</a>";
					if($legralUsers->AUisDomaineAut('sommaireEdit')){$out.=" - <a href='?page=editer&listeType=$listeType&article=$el'>éditer</a>";}
					$out.=')';
					}
				$out.='</li>';
				}
			}
		closedir($handle);
		}
	$out.='</ul>';
	return $out;
	}	//listeRep
// ===== Menu =====
//menu de selection du type
$out='<h1>'
.	'<a href="' .$_SERVER["SCRIPT_NAME"].'?listeType=textes">textes</a>'
.	' - <a href="'.$_SERVER["SCRIPT_NAME"].'?listeType=pages">pages</a>'
.	' intersites ('
.	'<a href="'.$_SERVER["SCRIPT_NAME"].'?listeType=intersitesLibPerso">perso</a>'
.	' - <a href="'.$_SERVER["SCRIPT_NAME"].'?listeType=intersitesLibTiers">tiers</a>'
.	' - <a href="'.$_SERVER["SCRIPT_NAME"].'?listeType=intersitesSitesTemplates">site-templates</a>'
.	')';
if($legralUsers->AUisDomaineAut('sommaireEdit'))$out.=' - <a href="'.$_SERVER["SCRIPT_NAME"].'?listeType=home">home</a>';
$out.='</h1>';
echo $out;

if($legralUsers->AUisDomaineAut('sommaireEdit'))
	{
	echo	'<form action="'.$_SERVER["REQUEST_URI"].'" method="post">'
	.	'<span title="Créer">Créer comme <input type="submit" name="mkdir"value="REPERTOIRE">, <input name="mkfic" type="submit" value="FICHIER">: <input name="cibleNom" type="text" size=50></span>'
	.	'</form><br>';
// ---- creation d'un repertoire ----
if(isset($_POST['mkdir']))
	{
	$mk=$_POST['mkdir'];
	$cibleNom=isset($_POST['cibleNom'])?$_POST['cibleNom']:NULL;
	$cibleNom=$_POST['cibleNom'];$cibleNom=str_replace('../','',$cibleNom);$cibleNom=str_replace('..\\','',$cibleNom);
	if(!$cibleNom)	{echo'<div class="notewarning">Aucune cible fournit!</div>';}
	else	{
		$d=FICHIER_ROOT.$cibleNom;
		echo $gestLib->debugShowVar('legral-sommaire',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'FICHIER_ROOT.$cibleNom',FICHIER_ROOT.$cibleNom);
		if (@mkdir($d,0770,true)){echo'<div class="notewarning">Création du répétoire impossible!</div>';}
		else{echo'<div class="noteclassic">Création du répétoire: ok!</div>';}
		}
	}

// ---- creation d'un fichier ----
if(isset($_POST['mkfic']))
	{
	$mk=$_POST['mkfic'];
	$cibleNom=isset($_POST['cibleNom'])?$_POST['cibleNom']:NULL;
	$cibleNom=$_POST['cibleNom'];$cibleNom=str_replace('../','',$cibleNom);$cibleNom=str_replace('..\\','',$cibleNom);
	if(!$cibleNom)	{echo'<div class="notewarning">Aucune cible fournit!</div>';}
	else	{
		$d=FICHIER_ROOT.$cibleNom;
	//	echo $gestLib->debugShowVar('legral-sommaire',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'FICHIER_ROOT.$cibleNom',FICHIER_ROOT.$cibleNom);
		$fh=@fopen($d,'w');
		if($fh)
			{
			echo'<div class="notewarning">Création du fichier ok!</div>';
			fclose($fh);
			}
		else{echo'<div class="noteclassic">Création du fichier: impossible!</div>';}
		}
	}
}	// if($legralUsers->AUisDomaineAut('sommaireEdit'))

echo	'<div class="liste">'.listeRep().'</div>';
echo	"nombre de rep:$sommaireRepNb<br>nombre de fic:$sommaireFicNb<br>";
?>
