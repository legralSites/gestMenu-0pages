<!DOCTYPE html><html><head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="articles n'en vracs">
<meta name="keywords"content="">
<meta name="generator" content="gEdit">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20130121">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">
<title>&eacute;diter</title><script src="/intersites/lib/tiers/js/jquery/jquery-1.8.1.min.js"></script>

<script>$.noConflict();</script>

<link rel="stylesheet" href="/intersites/styles/intersites.css">
<link rel="stylesheet" href="/intersites/lib/perso/php/gestLib/gestLib-0.1.css" type="text/css" media="all">

<link rel="stylesheet" href="/intersites/styles/notes/notes-0.1.css">
<link rel="stylesheet" href="/intersites/sites-templates/standart1.1/body-html4.css">
<link rel="stylesheet" href="./styles/template1.1/body.css">
<link rel="stylesheet" href="./styles/sommaire-menu-template-onglets.css">
</head><body>
<div id="page" class="pageLarg">
<div id="console_support"><span id="console_fermer"onclick="lgDebug.hide();">fermer</span><div id="console_titre">Console</div><div id="console_contenu"></div></div>
<script>
function lgDebug()
	{
	this.console_supportHTMLId=document.getElementById('console_support');
	this.console_titreHTMLId=document.getElementById('console_titre');
	this.console_contenuHTMLId=document.getElementById('console_contenu');
	this.hide=function(){this.console_supportHTMLId.style.display="none";}
	this.show=function(){this.console_supportHTMLId.style.display="block";}
	this.clear=function(){this.console_titreHTMLId='Console';this.console_contenuHTMLId.innerHTML='';}
	this.add=function(txt)
		{
		this.show();
		this.console_contenuHTMLId.innerHTML+=txt;
		}
	this.write=function(titre,txt)
		{
		this.show();
		this.console_titreHTMLId.innerHTML=titre;
		this.console_contenuHTMLId.innerHTML=txt;
		}
	};
lgDebug=new lgDebug();
</script>

<style>
#homeIcone{background:#111 url('/imageotheques/images/logoAVEC/avec-logo-1.03.png') no-repeat right top;width:15px;}
#homeIcone:hover{background:#111 url('/imageotheques/images/logoAVEC/avec-logo-1.03.png') no-repeat right top;width:15px;}
</style>
<div id="header" class="pageLarg">
<span id="header_gauche">
	<a href="/"><img id="homeIcone"alt="logoHome" src="/imageotheques/images/logoAVEC/avec-logo-1.03.png"/></a>
	<a href="?page=liste">&lt;&lt;page précedente</a>
</span>
<div id="header_droit">
<a href="?page=menuSysteme">menu Système</a>
<!--
<noscript><a href="?page=menu Système">menu Système</a></noscript>
<script>
document.write('<span class="pointeur" onclick="javascript:window.open();//page=menu Système">menu Système</span>');
</script>
-->
</div>
</div>
<h1>Sommaire des articles, des textes, et des pages (de code)</h1>


</body></html>

