<?php
function convertAccent($text)
{
///www/0pages/litteratures/outils
//	$text=htmlentities($text,ENT_NOQUOTES,'UTF-8');$text=htmlspecialchars_decode($text);return $text;
$carSpec=array('é','è','à','â','ô','î',"\n");
$htmlSpec=array('&eacute;','&egrave;','&agrave;','&acirc;','&ocirc;','&icirc;','<br>');
return str_replace($carSpec,$htmlSpec,$text);
}

$session_name='legral_session';
$session_nameOld= session_name($session_name);
session_start();
header('Content-type: text/html');header('Expires: 0');header('Cache-Control: must-revalidate');header('Pragma: public');

define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INDEX_ROOT',DOCUMENT_ROOT.'litteratures/litteratures-0.1/');
define('INTERSITES_ROOT',DOCUMENT_ROOT.'intersites/');
define('COLLECTION',isset($_POST['collection'])?$_POST['collection']:NULL);
define('TXT_ROOT',DOCUMENT_ROOT.'litteratures/txt/'.COLLECTION);
define('USER',isset($_SESSION['USER'])?$_SESSION['USER']:NULL);
define('LIVRENO',isset($_POST['livreNo'])?$_POST['livreNo']:NULL);
define('NIVEAU',isset($_POST['niveau'])?$_POST['niveau']:'S');//S:synopsis, R:resume
//if(USER===NULL){$output='Aucun utilisateur defini';exit($output);}

switch (NIVEAU)
		{
		case 'S':$niv='synopsis';break;
		case 'R':$niv='resume';break;
		}
define('FILENOM',TXT_ROOT.'livre'.LIVRENO."-$niv.txt");

$output='';

if (file_exists(FILENOM))
	{
	$taille=filesize(FILENOM);	$handle=fopen(FILENOM,"r");
	if($handle)
		{
		while(($buffer=fgets($handle,4096))!==false)
			{
			if(strpos($buffer,'#titre'>0)){$titre=$buffer;}
			else{$output.="$buffer";}
			}
		if (!feof($handle)) {$output.="Erreur: fgets() a échoué\n";}
		fclose($handle);
		}
	}
else {$output='impossible d\'ouvrir le fichier '.FILENOM.'<br>';}

$taille=strlen($output);
header('Content-Length: '.$taille);
echo convertAccent($output);
?>
