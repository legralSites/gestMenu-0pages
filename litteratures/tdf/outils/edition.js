function lgDebug()
	{
	this.console_supportHTMLId=document.getElementById('console_support');
	this.console_titreHTMLId=document.getElementById('console_titre');
	this.console_contenuHTMLId=document.getElementById('console_contenu');
	this.hide=function(){this.console_supportHTMLId.style.display="none";}
	this.show=function(){this.console_supportHTMLId.style.display="block";}
	this.clear=function(){this.console_titreHTMLId='Console';this.console_contenuHTMLId.innerHTML='';}
	this.add=function(txt)
		{
		this.show();
		this.console_contenuHTMLId.innerHTML+=txt;
		}
	this.write=function(titre,txt)
		{
		this.show();
		this.console_titreHTMLId.innerHTML=titre;
		this.console_contenuHTMLId.innerHTML=txt;
		}
	};


function backupTxt()
	{
	var request = jQuery.ajax(
		{url:'backupTxt.php',type:"POST",data: {user:utilisateur},dataType: "html",
		success:function(msg)
			{
			lgDebug.write('archivage des fichiers txts',msg);
			jQuery('#console_support').show(1500).delay(2500).fadeOut(1500);
			},
		fail:function(jqXHR,textStatus){alert("Request failed: "+textStatus)}
		})
	}
function readFile(fileNo)
	{
	var request = jQuery.ajax(
		{url: "readFile.php",type: "POST",data: {fileNo: fileNo},dataType: "html",
		success:function(msg)
			{
			jQuery("#"+fileNo+"_data").html(msg);
			<?php if (USER=='pascal'OR LOGINPASS){echo 'jQuery("#"+fileNo+"_resume_edit").html(msg);';}?>
			},
		fail:function(jqXHR,textStatus){alert("Request failed: "+textStatus)}
		})
	}

function saveEdit(fileNu)
	{
	var donnee=jQuery('textarea#'+fileNu+"_resume_edit").val();
	var request = jQuery.ajax(
		{
		url:"saveFile.php",
		type:"POST",
		data:{fileNo:fileNu,txt:donnee},
		dataType:"html",
		success:function(msg)
			{
			//jQuery("#"+fileNu+"_data").html(msg);
			//jQuery("#console").html(msg);
			lgDebug.write('sauvegarde',msg);
			jQuery('#console_support').show(1500).delay(2500).fadeOut(1500);
			readFile(fileNu);
			//alert(fileNu+' sauvé');
			},
		fail:function(jqXHR, textStatus){alert("Request failed: "+textStatus)}
		})
	}
