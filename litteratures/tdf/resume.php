<link rel="stylesheet" href="/intersites/lib/perso/js/lgMsgbox/lgMsgbox-0.1/lgMsgbox-0.1.css" type="text/css" media="all">
<?php
include INTERSITES_ROOT.'lib/perso/js/lgMsgbox/lgMsgbox-0.1/lgMsgbox-0.1.php';echo lgMsgBox_add('tdf');

define('TEXTES_ROOT_LOCAL',TEXTES_ROOT.'trone_de_fer/');


function livre_template($livreNu,$livreNom)
	{
	$livre_synopsis_data=lireLivre($livreNu,'S');
	$livre_resume_data=lirelivre($livreNu,'R');
	return
	'<div class="tdf_livre">'
		.'<div class="tdf_livre_titre">'
			."$livreNu - $livreNom"
		.'</div>'
		.'<div class="tdf_livre_synops_titre">Synopsis&nbsp;'
		.'<img alt="rafraichir"title="recharger le synopsis"src="/icons/back.gif" style="display:absolute;cursor:pointer"onclick="'."lireLivre('$livreNu','S');".'">'
			.'<div id="tdf_livre'.$livreNu.'_synops_content"class="tdf_livre_synops_content">'.$livre_synopsis_data.'</div>'
		.'</div>'
		.'<div class="tdf_livre_resume_titre">résumé&nbsp;'
		.'<img alt="rafraichir"title="recharger le résumé"src="/icons/back.gif" style="display:absolute;cursor:pointer"onclick="'.  "lireLivre('$livreNu','R');".'">'
			.'<div id="tdf_livre'.$livreNu.'_resume_content"class="tdf_livre_resume_content">'.$livre_resume_data.'</div>'
		.'</div>'

	//edition si ogin ok
/*
	if(USER=='pascal'or LOGINPASS)
		{echo'<img alt="sauver"src="/intersites/images/puces/b_inline_edit.png" class="track_edit_titre"onclick="saveEdit('.$titre.');"><br>'.
			'<textarea id="'.$titre.'_resume_edit"class="track_edit">'.$titre_data.'</textarea>';
			}
*/
	."</div>\n";
	}


function convertAccent($text)
{
$out=$text;
//	$text=htmlentities($text,ENT_NOQUOTES,'UTF-8');$text=htmlspecialchars_decode($text);return $text;
$carSpec=array('é','è','à','â','ô','î');
$htmlSpec=array('&eacute;','&egrave;','&agrave;','&acirc;','&ocirc;','&icirc;');
return str_replace($carSpec,$htmlSpec,$text);
}

function lireLivre($livreNu,$niveau='S')
	{
	$output='';
	$taille=0;
	switch ($niveau)
		{
		case 'S':$niv='synopsis';break;
		case 'R':$niv='resume';break;
		}

	$file=TEXTES_ROOT_LOCAL."livre$livreNu-$niv.txt";
	if(file_exists($file))
		{
		$taille=filesize($file);	$handle=fopen($file,"r");
		if($handle)
			{
			while(($buffer=fgets($handle,4096))!==false)
				{
				//if(strpos($buffer,'#titre'>0)){$titre=$buffer;}else{$output.=$buffer;}
				$output.=$buffer;
				}
			if (!feof($handle)) {$output.="Erreur: fgets() a échoué\n";}
			fclose($handle);
			}
		}
	else{$output.="fichier inexistant:<i>$file</i><br>";}
	$output=convertAccent($output);	
	$output.="<br><i>fileNo:.$livreNu";
	$output.="taille:.$taille</i>";
	return $output;
	}

function showPlayer($mp3)
	{$mp3=MP3_ROOT.$mp3.'.mp3';
	$out='<object id="'.$mp3.'_dewplayerjs" class="dewplayer"type="application/x-shockwave-flash" data="/intersites/lib/tiers/js/dew/dewplayer/dewPlayer-20120401/dewplayer.swf?mp3='.$mp3.'&amp;javascript=on" height="20" width="200">';
	$out.='<param name="wmode" value="transparent"><param name="movie" value="/intersites/lib/tiers/js/dew/dewplayer/dewPlayer-20120401/dewplayer.swf?mp3='.$mp3.'&amp;javascript=on">';
	$out.='</object>';
	return $out;
	}
/**********************************
partie Javascript
***********************************/

?><script>
function lireLivre(livreNu,niveau)
	{
	if(!livreNu){return-1;}
	niv='S';
	//ne selectionne que des parametres autorisés
	switch(niveau)
		{
		//case 'S':niv='synopsis';break;
		case 'R':niv='R';break;
		}
	var request = jQuery.ajax(
		{url:"pages/tdf/readFile.php",type:"POST",data:{livreNo:livreNu,niveau:niv},dataType:"html",
		success:function(msg)
			{
			jQuery('#tdf_livre'+livreNu+'_resume_content').html(msg);
			//jQuery("#console_contenu").html('livre '+livreNu+niv+' charg&eacute;');
			var msg='Rechargement du ';
			switch(niveau){case'S':msg+='';msg+='synopsis';break;case'R':msg+='résumé';break;}
			msg+=' du livre '+livreNu+' est recharg&eacute;'

			tdf_lgMsgBoxJS.write('lecture',msg,0);
			jQuery('#tdf_lgMsgBox_support').show(1500).delay(2500).fadeOut(1500);
			<?php if (USER=='pascal'OR LOGINPASS){echo 'jQuery("#"+livreNu+"_resume_edit").html(msg);';}?>
			},
		fail:function(jqXHR,textStatus){alert("Request failed: "+textStatus)}
		})
	}

function saveEdit(fileNu)
	{
	var donnee=jQuery('textarea#'+fileNu+"_resume_edit").val();
	var request = jQuery.ajax(
		{
		url:"saveFile.php",
		type:"POST",
		data:{fileNo:fileNu,txt:donnee},
		dataType:"html",
		success:function(msg)
			{
			//jQuery("#"+fileNu+"_data").html(msg);
			//jQuery("#console_contenu").html(msg);
			//lgDebug.write('sauvegarde',msg);
			//jQuery('#console_support').show(1500).delay(2500).fadeOut(1500);
			readFile(fileNu);
			//alert(fileNu+' sauvé');
			},
		fail:function(jqXHR, textStatus){alert("Request failed: "+textStatus)}
		})
	}
</script>
<link rel="stylesheet" href="../0pages/litteratures/tdf/resume.css">
<!--div id="console_support"><span id="console_fermer"onclick="lgDebug.hide();">fermer</span><div id="console_titre">Console</div><div id="console_contenu"></div></div-->
<?php
echo
	'<div class="integral">'.livre_template(0,'Le Chevalier Errant').livre_template(0.1,'L\'&Eacute;p&eacute;e Lige').'</div>'
.	'<div class="integral">'.livre_template(1,'Le trone de Fer').livre_template(2,'le Donjon Rouge').'</div>'
.	'<div class="integral">'.livre_template(3,'La Bataille des Rois').livre_template(4,'l\'Ombre Mal&eacute;fique').livre_template(5,'l\'Invincible Forteresse').'</div>'
.	'<div class="integral">'.livre_template(6,'Intrigue &agrave; Port-R&eacute;al').livre_template(7,'l\'&Eacute;p&eacute;e de Feu').livre_template(8,'les Noces Pourpres').'</div>'
.	'<div class="integral">'.livre_template(9,'La Loi du R&eacute;gicide').livre_template(10,'').livre_template(11,'Le Chaos').livre_template(12,'Un Festin Pour Les Corbeaux').'</div>'
.	'<div class="integral">'
;
?>
