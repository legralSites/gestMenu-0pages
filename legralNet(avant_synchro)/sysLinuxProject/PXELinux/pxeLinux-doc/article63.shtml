<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<html><head>
 <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
 <meta name="GENERATOR" content="lfparser_2.32">
 <meta name="LFCATEGORY" content="SystemAdministration">
 <link rel="icon" href="http://www.linuxfocus.org/common/images/lf-16.png" type="image/png">
 <title>lf63, SystemAdministration: An Introduction to Network Booting and Etherboot</title>
<style type="text/css">
<!--
 td.top {font-family: Arial,Geneva,Verdana,Helvetica,sans-serif; font-size:12 }
 pre { font-family:monospace,Courier }
 p.cl { color:#EE9500 }
 a.nodec { text-decoration:none }
 p.trans { font-size:8pt; text-align:right }
 p.clbox { width:50%; alignment:center; background-color:#FFD700; 
           border-style:none; border-width:medium; border-color:#FFD700; 
           padding:0.5cm;  text-align:center }
 p.code { width:80%; alignment:center; background-color:#aedbe8; 
          border-style:none; border-width:medium; border-color:#aedbe8; 
          padding:0.1cm;  text-align:left }
 p.foot { background-color:#AAAAAA; color:#FFFFFF; border-style:none; 
          border-width:medium; border-color:#AAAAAA; padding:0.5cm ; 
          margin-top:0.1cm; margin-right:1cm; margin-left:1cm; 
          text-align:center }
 .mark  { background-color:#e6e6ff }
-->
</style>
 
</head>
<body bgcolor="#ffffff" text="#000000">
 <!-- this is generated html code. NEVER use this file for your
 translation work. Instead get the file with the same article number
 and .meta.shtml in its name. Translate this meta file and then
 use lfparser program to generate the final article -->
 <!-- lfparser can be obtained from http://www.linuxfocus.org/~guido/dev/lfparser.html -->

<!-- this is used by a number of tools:
 =LF=AUTHOR: Ken Yap
 =LF=CAT___: SystemAdministration
 =LF=TITLE_: An Introduction to Network Booting and Etherboot
 =LF=NUMBER: 63
 =LF=ANAME_: article63.shtml
 -->

<!-- 2pdaIgnoreStart -->

<!-- start navegation bar, old style -->
<map name="top">
  <area shape="rect" coords="367,9,418,30" href="http://www.linuxfocus.org/English/">
  <area shape="rect" coords="423,9,457,30" href="http://www.linuxfocus.org/English/map.html">
  <area shape="rect" coords="463,9,508,30" href="http://www.linuxfocus.org/English/indice.html">
  <area shape="rect" coords="514,9,558,30" href="http://www.linuxfocus.org/English/Search/">
</map>
<map name="bottom">
  <area shape="rect" coords="78,0,163,15" href="http://www.linuxfocus.org/English/News/">
  <area shape="rect" coords="189,0,284,15" href="http://www.linuxfocus.org/English/Archives/">
  <area shape="rect" coords="319,0,395,15" href="http://www.linuxfocus.org/English/Links/">
  <area shape="rect" coords="436,0,523,15" href="http://www.linuxfocus.org/English/aboutus.html">
</map>
<!-- IMAGE HEADER -->
<center>
  <img src="article63_fichiers/Topbar-en.gif" alt="[Top bar]" ismap="ismap" usemap="#top" height="40" border="0" width="600"><br>
  <img src="article63_fichiers/Bottombar-en.gif" alt="[Bottom bar]" ismap="ismap" usemap="#bottom" height="21" border="0" width="600">
</center>
<!-- stop navegation bar, old style -->

<!-- SSI_INFO -->

<!-- addedByLfdynahead ver 1.5 --><table align="right" border="0"><tbody><tr><td align="right"><font face="Arial,Helvetica" size="-1">This document is available in: <a href="http://www.linuxfocus.org/English/September1998/article63.shtml">English</a> &nbsp;<a href="http://www.linuxfocus.org/Castellano/September1998/article63.html">Castellano</a> &nbsp;<a href="http://www.linuxfocus.org/Deutsch/September1998/article63.html">Deutsch</a> &nbsp;<a href="http://www.linuxfocus.org/Francais/September1998/article63.html">Francais</a> &nbsp;<a href="http://www.linuxfocus.org/Nederlands/September1998/article63.shtml">Nederlands</a> &nbsp;<a href="http://www.linuxfocus.org/Portugues/September1998/article63.html">Portugues</a> &nbsp;<a href="http://www.linuxfocus.org/Russian/September1998/article63.html">Russian</a> &nbsp;<a href="http://www.linuxfocus.org/Turkce/September1998/article63.html">Turkce</a> &nbsp;<a href="http://www.linuxfocus.org/Korean/September1998/article63.html">Korean</a> &nbsp;</font></td></tr></tbody></table><br>
 


<!-- SSI_INFO STOP -->
<!-- 2pdaIgnoreStop -->

<!-- SHORT BIO ABOUT THE AUTHOR -->
<table summary="about the author" align="LEFT" border="0" width="190">
<tbody><tr>
<td>

<img src="article63_fichiers/Ken-Y.gif" alt="[Photo of the Author]" height="164" width="173">
<br>by  Ken Yap <br> <small>&lt;ken_yap(at)users.sourceforge.net&gt;</small>
<br><br>
<i>About the author:</i><br>

Ken lives in Sydney, Australia. He first encountered Unix in 1979
and has been using Linux for the last 4 years for word processing,
Internet access and supporting his electronics hobby. When not playing
with Linux or at his day job, he likes to travel, meet interesting people,
try new cuisines or just enjoy the surroundings while strolling. He
works at a multinational company as a research scientist.


<!-- TRANSLATED TO en -->
<!-- TRANSLATED TO STOP -->
<br><i>Content</i>:
<ul>
  <li><a href="#63lfindex0">What is network booting?</a></li>
  <li><a href="#63lfindex1">How does it work?</a></li>
  <li><a href="#63lfindex2">Netbooting in Practice</a></li>
  <li><a href="#63lfindex3">Uses of Network Booting</a></li>
  <li><a href="#63lfindex4">For More Information</a></li>
</ul>

</td></tr></tbody></table>
<!-- HEAD OF THE ARTICLE -->
<br>&nbsp;
<h2>An Introduction to Network Booting and Etherboot</h2>
 <img src="article63_fichiers/illustration2.gif" alt="[Illustration]" height="100" hspace="10" width="100">
<!-- ABSTRACT OF THE ARTICLE -->
<p><i>Abstract</i>:
</p><p>
<font face="Helvetica,Arial,Roman" size="2">
This is an advanced article that
explains how to bootstrap your
computer from a program stored
in non-volatile memory without
accessing your hard disk. It is an
ideal technique for maintaining and
configure a farm of linux boxes</font></p>
<hr align="right" noshade="noshade" size="2"><br>
<!-- BODY OF THE ARTICLE -->


<a name="63lfindex0">&nbsp;</a>
<h3>What is network booting?</h3>


<p>Network booting is an old idea. The central idea is that the
computer has some bootstrap code in non-volatile memory, e.g. a ROM
chip, that will allow it to contact a server and obtain system
files over a network link. One goal is to avoid the use of a hard
disk for booting. There are various reasons for doing this. One is
to reduce the cost of maintaining software on many different
machines. With network booting the files are held at a central
server and can be updated at one location. Another goal is to use
computers in locations where hard disks are not robust enough. For
example this might be a computer on a factory floor where a hard
disk might be too fragile. Finally another goal is to have a system
that can be switched between different operating systems without
having to reload the software.

</p><p>Network booting often co-exists with disk booting. For example,
a system could run Windows from disk but sometimes boot Linux from
the network. There are some interesting applications of this
technique. For example: a friend of mine uses this to reload
Windows over the network. When a Windows installation becomes
corrupted, as it often does, the sysadmin can reload a fresh
installation by booting Linux over the network and letting the
automatic script format the disk and copy a fresh Windows
installation onto it.</p>

<a name="63lfindex1">&nbsp;</a>
<h3>How does it work?</h3>


<p>In order to boot over the network, the computer must get 1. an
identity, 2. an operating system image and 3. usually, a working
filesystem.

</p><p>Consider a diskless computer (DC) that has a network boot ROM.
It may be one of several identical DCs. How can we distinguish this
computer from others? There is one piece of information that is unique
to that computer (actually its network adapter) and that is its Ethernet
address. Every Ethernet adapter in the world has a unique 48 bit Ethernet
address because every Ethernet hardware manufacturer has been assigned
blocks of addresses. By convention these addresses are written as hex
digits with colons separating each group of two digits, for example:
<tt>00:60:08:C7:A3:D8</tt>.</p>

<p>The protocols used for obtaining an IP address, given an
Ethernet address, are called Boot Protocol (BOOTP) and Dynamic Host
Configuration Protocol (DHCP). DHCP is an evolution of BOOTP. In
our discussion, unless otherwise stated, anything that applies to
BOOTP also applies to DHCP. (Actually it's a small lie that BOOTP
and DHCP only translate Ethernet addresses. In their foresight, the
designers made provision for BOOTP and DHCP to work with any kind
of hardware address. But Ethernet is what most people will be
using.)</p>

<p>An example of a BOOTP exchange goes like this:</p>

<p>DC: Hello, my hardware address is <tt>00:60:08:C7:A3:D8</tt>, please
give me my IP address.</p>

<p>BOOTP server: (Looks up address in database.) Your name is
<tt>aldebaran</tt>, your IP address is 192.168.1.100, your server
is <tt>192.168.1.1</tt>, the file you are supposed to boot from is
<tt>/tftpboot/vmlinux.nb</tt> (and a few other pieces of information).</p>

<p>You may wonder how the DC found the address of the BOOTP server
in the first place. The answer is that it didn't. The BOOTP request
was broadcast on the local network and any BOOTP server that can
answer the request will.</p>

<p>After obtaining an IP address, the DC must download an operating
system image and execute it. Another Internet protocol is used
here, called Trivial File Transfer Protocol (TFTP). TFTP is like a
cut-down version of FTP---there is no authentication, and it runs
over User Datagram Protocol (UDP) instead of Transmission Control Protocol
(TCP). UDP was chosen instead of TCP for simplicity. The implementation of
UDP on the DC can be small so the code is easy to fit on a ROM. Because
UDP is a block oriented, as opposed to a stream oriented, protocol,
the transfer goes block by block, like this:</p>

<p>DC: Give me block 1 of <tt>/tftpboot/vmlinux.nb</tt>.<br>
TFTP server: Here it is.<br>
DC: Give me block 2.</p>

<p>and so on, until the whole file is transferred. Handshaking is a simple
acknowledge each block scheme, and packet loss is handled by retransmit
on timeout. When all blocks have been received, the network boot ROM
hands control to the operating system image at the entry point.</p>

<p>Finally, in order to run an operating system, a root filesystem
must be provided. The protocol used by Linux and other Unixes is
normally Network File System (NFS), although other choices are
possible. In this case the code does not have to reside in the ROM
but can be part of the operating system we just downloaded. However
the operating system must be capable of running with a root
filesystem that is a NFS, instead of a real disk. Linux has the
required configuration variables to build a version that can do
so.</p>

<a name="63lfindex2">&nbsp;</a>
<h3>Netbooting in Practice</h3>


<p>Besides commercial boot ROMs, there are two sources for free packages
for network booting. They are Etherboot and Netboot. Both can be found
through the <a href="http://etherboot.sourceforge.net/">Etherboot
home page</a>.  First you have to ascertain that your network card is
supported by Etherboot or Netboot.  Eventually you have to find a person
who is willing to put the code on an EPROM (Erasable Programmable Read
Only Memory) for you but in the beginning you can do network booting
from a floppy.

</p><p>To create a boot floppy, a special boot block is provided in the
distribution. This small 512 byte program loads the disk blocks following
it on the floppy into memory and starts execution.  Thus to make a boot
floppy, one has only to concatenate the boot block with the Etherboot
binary containing the driver for one's network card like this:</p>

<p><tt>cat floppyload.bin 3c509.lzrom &gt; /dev/fd0</tt></p>

<p>Before you put in the network boot floppy, you have to set up
three services on Linux: BOOTP (or DHCP), TFTP and NFS. You don't
have to set up all three at once, you can do them step by step,
making sure each step works before going on to the next.</p>

<p>I assume you have installed the bootpd server from a
distribution or by compiling from source. You then have to ensure
that this server is waiting for bootp requests. There are two ways
to do this: one is to start bootpd as a network service that is
always listening when the computer is up, and the other is to start
it from inetd. For the latter, <tt>/etc/inetd.conf</tt> should
contain a line like this:</p>

<p><tt>bootps dgram udp wait root /usr/sbin/tcpd bootpd</tt></p>

<p>If you had to modify <tt>/etc/inetd.conf</tt>, then you need to
restart inetd by sending the process a HUP signal.</p>

<p>Next, you need to give bootp a database to map Ethernet
addresses to IP addresses. This database is in <tt>
/etc/bootptab.</tt> It contains lines of the following form:</p>

<font size="-1">
<tt>
aldebaran.foo.com:ha=006008C7A3D8:ip=192.168.1.100:bf=/tftpboot/vmlinuz.nb
</tt>
</font>

<p>Other information can be specified but we will start simple.</p>

<p>Now boot the DC with the floppy and it should detect your Ethernet card
and broadcast a BOOTP request. If all goes well, the server should respond
to the DC with the information required. Since <tt>/tftpboot/vmlinux.nb</tt>
doesn't exist yet, it will fail when it tries to load the file.</p>

<p>Now you need to compile a special kernel, one that has the
option for mounting the root filesystem from NFS turned on. You
also need to enable the option to get the IP address of the kernel
from the original BOOTP reply. You also need to compile the Linux
driver for your network adapter into the kernel instead of loading
it as a module. It is possible to download an initial ramdisk so
that module loading works but this is something you can do
later.</p>

<p>You cannot install the zImage resulting from the kernel compilation
directly. It has to be turned into a <i>tagged image</i>. A tagged image
is a normal kernel image with a special header that tells the network
bootloader where the bytes go in memory and at what address to start the
program. You use a program called <tt>mknbi-linux</tt> to create this tagged
image. This utility can be found in the Etherboot distribution. After
you have generated the image, put it in the <tt>/tftpboot</tt> directory
under the name specified in <tt>/etc/bootptab</tt>. Make sure to make
this file world readable because the tftp server does not have special
privileges.</p>

<p>For TFTP, I assume that you installed tftpd from a distribution
or by compiling from source. Tftpd is normally started up from
inetd with a line like this in <tt>/etc/inetd.conf</tt>.</p>

<p><tt>tftp dgram udp wait root /usr/sbin/tcpd in.tftpd -s
/tftpboot</tt></p>

<p>Again, restart inetd with a HUP signal and you can retry the
boot and this time it should download the kernel image and start
it. You will find that the boot will continue until the point where
it tries to mount a root filesystem. At this point you must
configure and export NFS partitions to proceed.</p>

<p>For various reasons, it's not a good idea to use the root
filesystem of the server as the root filesystem of the DCs. One is
simply that there are various configuration files there and the DC
will get the wrong information that way. Another is security. It's
dangerous to allow write access (and write access is needed for the
root filesystem, for various reasons) to your server's root.
However the good news is that a root filesystem for the DC is not
very large, only about 30 MB and a lot of this can be shared
between multiple DCs.</p>

<p>Ideally, to construct a root filesystem, you have to know what
files your operating system distribution is expecting to see there.
Critical to booting are device files, files in <tt>/sbin</tt> and
<tt>/etc</tt>. You can bypass a lot of the hard work by making a copy of
an existing root filesystem and modifying some files for the DC.  In the
Etherboot distribution, there is a tutorial and links to a couple of shell
scripts that will create such a DC root filesystem from an existing server
root filesystem. There are also troubleshooting tips in the Etherboot
documentation as this is often the trickiest part of the setup.</p>

<p>The customised Linux kernel for the DC expects to see the root
filesystem at <tt>/tftpboot/</tt><i>&lt;IP address of the DC&gt;</i>,
for example: <tt>/tftpboot/192.168.1.100</tt> in the case above. This
can be changed when configuring the kernel, if desired.</p>

<p>Now create or edit <tt>/etc/exports</tt> on the server and put in a
line of the following form:</p>

<p><tt>/tftpboot/192.168.1.100
aldebaran.foo.com(rw,no_root_squash)</tt></p>

<p>The <tt>rw</tt> access is needed for various system services. The
<tt>no_root_squash</tt> attribute prevents the NFS system from mapping
root's ID to another one. If this is not specified, then various daemons
and loggers will be unhappy.</p>

<p>Start or restart the NFS services (rpc.portmap and rpc.mountd)
and retry the diskless boot. If you are successful, the kernel should
be able to mount a root filesystem and boot all the way to a login
prompt. Most likely, you will find several things misconfigured. Most
Linux distributions are oriented towards disked operation and require a
little modification to suit diskless booting. The most common failing is
reliance on files under /usr during the boot process, which is normally
imported from a server late in the boot process.  Two possible solutions
are: 1. Provide the few required files under a small /usr directory on
the root filesystem, which will then be overlaid when /usr is imported,
and 2. Modify the paths to look for the files in the root filesystem. The
files to edit are under <tt>/tftpboot/192.168.1.100</tt> (remember,
this is the root directory of the DC).</p>

<p>You may wish to mount other directories from the server, such as
<tt>/usr</tt> (which can be exported read-only).</p>

<p>When you are satisfied that you can boot over the network without any
problems, you may wish to put the code on an EPROM.  An EPROM programmer
starts at around $100 US, and is not a cost effective investment for
a hobbyist who uses it sporadically.  Occasionally one will appear on
the used market at a bargain price, the main caveat being to ensure that
the software to drive it is available. A proficient electronics hobbyist
could build one using one of the several free designs published on the
Internet, but for the majority of readers, the best solution is to make
the acquaintance of someone who has access to one, perhaps someone in
an electronics hobbyist group or working in the electronics industry.</p>

<p>A short note on EPROM technology: The bits of an EPROM are programmed
by injecting electrons with an elevated voltage into the floating gate
of a field-effect transistor where a 0 bit is desired. The electrons
trapped there cause that transistor to conduct, reading as 0. To erase
the EPROM, the trapped electrons are given enough energy to escape the
floating gate by bombarding the chip with ultraviolet radiation through
the quartz window. To prevent slow erasure over a period of years from
sunlight and fluorescent lights, this quartz window is covered with an
opaque label in normal use.</p>

<p>There is another technology, called EEPROM or Electrically Erasable
PROM, sometimes called Flash PROM. Here the bits are cleared by an
electrical signal. This obviates the need for an ultraviolet eraser if the
EPROM is to be reused, but requires additional circuitry to support the
erase phase. If one is handy with electronics, there is a contributed
circuit design and driver software for an EEPROM board in the Etherboot
distribution. The board is plugged into any spare ISA bus slot on a PC
and boots a network card plugged into some other slot.</p>

<a name="63lfindex3">&nbsp;</a>
<h3>Uses of Network Booting</h3>


<p>X-terminals are one natural use of network booting. The lack of a
disk in the terminal makes it quieter and contributes to a pleasant
working environment. The machine should ideally have 16MB of memory
or more and the best video card you can find for it. This is an ideal
use for a high-end 486 or low-end Pentium that has been obsoleted by
hardware advances.</p>

<p>Other people have used network booting for clusters of machines
where the usage is light on the DC and does not warrant a disk,
e.g. a cluster of classroom machines.</p>

<a name="63lfindex4">&nbsp;</a>
<h3>For More Information</h3>


Your first stop should be the Etherboot home page:<br>
<tt><a href="http://etherboot.sourceforge.net/">http://etherboot.sourceforge.net</a></tt>

<p>There you will find links to other resources, including a mailing list
you can subscribe to, where problems and solutions are discussed.</p>

<p>Happy netbooting!</p>



<!-- vim: set sw=2 ts=2 et: -->

<!-- 2pdaIgnoreStart -->
<hr noshade="noshade" size="2">
<!-- ARTICLE FOOT -->
<center><table summary="footer" width="98%">
<tbody><tr><td align="CENTER" bgcolor="#9999AA" width="50%">
<a href="http://www.linuxfocus.org/common/lfteam.html">Webpages maintained by the LinuxFocus Editor team</a>
<br><font color="#FFFFFF">� Ken Yap, <a href="http://www.linuxfocus.org/common/copy.html">FDL</a> <br><a href="http://www.linuxfocus.org/">LinuxFocus.org</a></font>
</td>
<td bgcolor="#9999AA">
<!-- TRANSLATION INFO -->
<font size="2">Translation information:</font>
<table summary="translators">
  <tbody><tr><td><font size="2">en --&gt; -- : Ken Yap <small>&lt;ken_yap(at)users.sourceforge.net&gt;</small></font></td></tr>
</tbody></table>
</td>
</tr></tbody></table></center>
<p><font size="1">2002-10-20, generated by lfparser version 2.32</font></p>
<!-- 2pdaIgnoreStop -->


</body></html>