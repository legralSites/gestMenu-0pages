<h3>Le projet SysLinux</h3>
<p>
SysLinux est un systeme de menu pour charger un programme tel que OS ou programme de bas niveau ou image_Live.Il a plusieurs composants:
<ul>
	<li>comprenant divers modules de bas niveau notamment un système de menu menuvesa.c32</li>
	<li>les bootloaders selon le systeme de fichiers ou ils sont intallés:
		<ul>
			<li>SYSLinux: fs: fat32(vfat)</li>
			<li>EXTLinux: fs:ext2</li>
			<li>ISOLinux: CDROM</li>
			<li>PXELinux: reseau</li>
		</ul>
		<ul>
			<li>MEMDisk: fsSupport: indépendant. Emule un peripherique (floppy,CDROM,HD) pour le PC et mount une image contenant un système de fichier de meme type(.img .iso) en mémoire</li>
		</ul>
	</li>
</ul>
Références:
<ul>
	<li><a href="http://www.syslinux.org">http://www.syslinux.org</a></li>
	<li><a href="http://man7.org/linux/man-pages/man4/initrd.4.html">intrd</a></li>
</ul>


</p>

