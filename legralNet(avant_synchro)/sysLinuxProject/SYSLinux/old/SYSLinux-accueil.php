<h3>SysLinux</h3>
<p>
<ul>
	<li><a href="presentation.htm"></a>Présentation</li>
	<li><a href="exempleMenu-legacy.htm">exempleMenu-legacy</a></li>

</li>
</ul>
</p>

SysLinux est un système de menu pour charger un programme tel que OS ou programme de bas niveau ou image_Live. Il est utilisable sou la même forme:
<ul>
	<li>en local: il est sur le même support qu</li>
	<li>à distance: est apporter par un serveur via PXE</li>
</ul>

<h4>paramétrage du kernel via append</h4>
syntaxe: append option1 option2
<ul>
	<li>time: Show timing data on every kernel log message.</li>
	<li>quiet: Disable all log messages.</li>
	<li>loglevel</li>
</ul>
Ramdisk Options
<ul>
	<li>initrd: Location of initial ramdisk: initrd=filename</li>
	<li>load_ramdisk: Load a kernel ramdisk from a floppy: .load_ramdisk=n</li>
	<li>noinitrd: Do not use any initrd</li>
	<li>prompt_ramdisk: Prompt for the list of ramdisks: prompt_ramdisk=1</li>
	<li>ramdisk_blocksize: Blocksize of the ramdisk: ramdisk_blocksize=n</li>
	<li>ramdisk_size: Size of the ramdisk: ramdisk_size=n (en ko)</li>
</ul>

Root Disk Options
<ul>
	<li>ro: Mount the root device read-only on boot.</li>
	<li>root: Specify the root filesystem to boot from: root=device<br>
Tell the kernel which disk device the root filesystem image is on.
device can be specified in one of the following ways:<br>

	<u>nnnn</u><br>
A device number in hexadecimal represents the major and
minor number of the device in the internal format that the
kernel expects. This method is not recommended unless you
have access to kernel internals.<br>
<u>/dev/nfs</u><br>
Use the NFS disk specified by the nfsroot boot option as the
root disk.<br>
<u>/dev/<i>diskname</i></u><br>
Use the kernel disk name specified by <diskname> as the root
disk.<br>

<u>/dev/<i>diskname</i> <i>decimal</i></u><br>
Use the kernel disk name specified by <diskname> and the
partition specified by <decimal> as the root disk.<br>

<u>/dev/<i>diskname</i> p <i>decimal</i></u><br>
Use the kernel disk name specified by <diskname> and the
partition specified by <decimal> as the root disk. This is the
same as above, but is needed when <diskname> ends with a
digit.<br>
</li>
	<li>rootdelay: Time to delay before attempting to mount the root filesystem en seconde</li>
	<li>rootflags: The root filesystem mount options.<br>
<i>rootflags=options</i><br>
Mount options that the kernel should use in mounting the root filesystem.
The options value depend on the filesystem type; see the
documentation for the individual types for details on what is valid.</li>

	<li>rootfstype: The root filesystem type: rootfstype=type</li>
	<li>rw: Mount the root device read-write on boot.</li>
	<li></li>
	<li></li>
	<li></li>
</ul>

Init Options
<ul>
	<li>init: Program to run at init time: init=filename<br>
Run the specified binary as the init process instead of the default<br>
/sbin/init program.</li>

	<li>rdinit: Run the init process from the ramdisk: rdinit=full_path_name<br>
Run the program specified by full_path_name as the init process.<br>
This file must be on the kernel ramdisk instead of on the root filesystem.</li>

	<li>S: Run init in single-user mode.</li>
</ul>


kexec Options
</ul>
	<li></li>
</ul>
ACPI Options
</ul>
	<li>acpi: ACPI subsystem options: acpi=[force|off|noirq|ht|strict]</li>
	<li>acpi_sleep: ACPI sleep options: acpi_sleep=[s3_bios],[s3_mode]</li>
	<li>acpi_sci:ACPI System Control Interrupt trigger mode:acpi_sci=[level|edge|high|low]</li>
	<li>...</li>
	<li></li>
</ul>
SCSI Options
</ul>
	<li>...</li>
</ul>

PCI Options
</ul>
	<li>...</li>
</ul>
Plug and Play BIOS Options
</ul>
	<li>...</li>
</ul>
SELinux Options
</ul>
	<li>...</li>
</ul>
Network Options
</ul>
	<li>...</li>
</ul>
Network File System Options
</ul>
	<li>nfsroot: Specifies the NFS root filesystem: nfsroot=[server-ip:]root-dir[,nfs-options]<br>
Set the NFS root filesystem for diskless boxes, to enable them to boot properly over NFS. If this parameter is not set, the value /tftpboot/client_ip_address will be used as the root filesystem with the default NFS options.<br>
<u>server-ip</u>: IP address of the NFS server to connect to.
<u>root-dir</u>: Directory on the NFS server to mount as root. If there is a %s token in this string, it will be replaced with the ASCII representation of the client’s IP address.
<u>nfs-options</u>: The standard NFS options, such as ro, separated by commas.</li>
</ul>
Hardware-Specific Options
</ul>
	<li>nousb: Disable the USB subsystem.</li>
	<li>lp: Parallel port and its mode: lp=[0|port[,port...]|reset|auto]</li>
	<li>parport: Specify the parallel port parameters: parport=[setting[,setting...]</li>
	<li>parport_init: mode: Parallel port initialization mode: parport_init_mode=[spp|ps2|epp|ecp|ecpepp]
	<li>...</li>
</ul>
Timer-Specific Options
</ul>
	<li>panic:Time to wait after panic before rebooting: panic=n	</li>
	<li>...</li>
</ul>
Miscellaneous Options
</ul>
	<li>...</li>
</ul>

<h4>Références:</h4>
<ul>
	<li>http://guim.info/dokuwiki/debian/pxelinux?do=index</li>
	<li>http://www.syslinux.org/wiki/index.php/PXELINUX</li>
	<li><a href="http://files.kroah.com/lkn/lkn_pdf/ch09.pdf" href="_blank">paramétrage du kernel via append</a></li>
</ul>


