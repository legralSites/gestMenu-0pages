<?php
// ==== selection de la page ====
$SYSLinux_gestPage=new gestPages('SYSLinux','accueil',PAGES_ROOT.'sysLinuxProject/SYSLinux/SYSLinux-accueil.html','SYSLinux');
	$SYSLinux_gestPage->setAttr('accueil','menu',1);
	$SYSLinux_gestPage->setAttr('accueil','classe','classPage');
	$SYSLinux_gestPage->setAttr('accueil','menuTitre','accueil');
	$SYSLinux_gestPage->setAttr('accueil','title','mon reseau - accueil');



$SYSLinux_gestPage->addPage('parametresGeneraux',PAGES_ROOT.'sysLinuxProject/SYSLinux/parametresGeneraux.html');
$SYSLinux_gestPage->setAttr('parametresGeneraux','menu',1);
$SYSLinux_gestPage->setAttr('parametresGeneraux','classe','classPage');

$SYSLinux_gestPage->setAttr('parametresGeneraux','menuTitre','param&egrave;tres G&eacute;n&eacute;raux');
$SYSLinux_gestPage->setAttr('parametresGeneraux','title','param&egrave;tres G&eacute;n&eacute;raux');



// sous menu lancer une image
$SYSLinux_gestPage->addPage('lancer_une_image',PAGES_ROOT.'sysLinuxProject/SYSLinux/lancer_une_image.html');
$SYSLinux_gestPage->setAttr('lancer_une_image','menu',1);

$SYSLinux_gestPage->setAttr('lancer_une_image','classe','classPage');
$SYSLinux_gestPage->setAttr('lancer_une_image','menuTitre','lancer une image');

$SYSLinux_gestPage->setAttr('lancer_une_image','title','SYSLinux - lancer une image');

$SYSLinux_gestPage->addPage('lancerMemDisk',PAGES_ROOT.'sysLinuxProject/SYSLinux/lanceur-memDisk.html');
$SYSLinux_gestPage->setAttr('lancerMemDisk','menu',1);

$SYSLinux_gestPage->setAttr('lancerMemDisk','classe','classPage');
$SYSLinux_gestPage->setAttr('lancerMemDisk','menuTitre','memDisk');

$SYSLinux_gestPage->setAttr('lancerMemDisk','title','SYSLinux - lancer une image via le module memDisk');

$SYSLinux_gestPage->addPage('lancerExemple',PAGES_ROOT.'sysLinuxProject/SYSLinux/lanceur-exemple.html');
$SYSLinux_gestPage->setAttr('lancerExemple','menu',1);

$SYSLinux_gestPage->setAttr('lancerExemple','classe','classPage');
$SYSLinux_gestPage->setAttr('lancerExemple','menuTitre','exemple en vrac');

$SYSLinux_gestPage->setAttr('lancerExemple','title',"SYSLinux - exemple de lancement d'images (non test�)");

$SYSLinux_gestPage->addPage('aideEnVrac',PAGES_ROOT.'sysLinuxProject/SYSLinux/aideEnVrac.html');
$SYSLinux_gestPage->setAttr('aideEnVrac','menu',1);

$SYSLinux_gestPage->setAttr('aideEnVrac','classe','classPage');
$SYSLinux_gestPage->setAttr('aideEnVrac','menuTitre','aide en vrac');

$SYSLinux_gestPage->setAttr('aideEnVrac','title',"SYSLinux - texte/source d'aide en vrac (non test�)");

$SYSLinux_gestPage->addPage('exempleMenu-legacy',PAGES_ROOT.'sysLinuxProject/SYSLinux/exempleMenu-legacy.html');
$SYSLinux_gestPage->setAttr('exempleMenu-legacy','menu',1);
$SYSLinux_gestPage->setAttr('exempleMenu-legacy','classe','classPage');
$SYSLinux_gestPage->setAttr('exempleMenu-legacy','menuTitre','ancienne syntaxe');

$SYSLinux_gestPage->setAttr('exempleMenu-legacy','title','SYSLinux - ancienne syntaxe');

$SYSLinux_gestPage->addPage('appendLinux',PAGES_ROOT.'sysLinuxProject/SYSLinux/append-linux.html');
$SYSLinux_gestPage->setAttr('appendLinux','menu',1);

$SYSLinux_gestPage->setAttr('appendLinux','classe','classPage');
$SYSLinux_gestPage->setAttr('appendLinux','menuTitre','append(linux)');

$SYSLinux_gestPage->setAttr('appendLinux','title','SYSLinux - les options pour append pour modifier le kernel linux');






$SYSLinux_menuOnglets=new menuOngletsV2('SYSLinux_menuOnglets');
foreach ($SYSLinux_gestPage->toArray('menu',1) as $key => $pageIndex)
	{
	$SYSLinux_menuOnglets->addA($pageIndex,$SYSLinux_gestPage->getAttr($pageIndex,'menuTitre'),"?SYSLinux=$pageIndex",$SYSLinux_gestPage->getURL($pageIndex),$SYSLinux_gestPage->getAttr($pageIndex,'classe'));
	}
global $historiqueMenu;
$historiqueMenu->addMenu($SYSLinux_gestPage->gestNom,$SYSLinux_gestPage->getPageActuelle());
?>

